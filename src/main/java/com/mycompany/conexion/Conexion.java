/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.conexion;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Tefa
 */
public class Conexion {
    
    // Atributos
    String url="jdbc:mysql://localhost:3306/agendaips";
    String user="root";
    String password= "Javeriana2016";

    // Constructor
    public Conexion(){
        // jdbc:mysql protocolo de conexión a la DB 
        // luego viene el dominio - localhost -> :3306 es el puerto    
    }
    
    // Métodos
    
    public Connection iniciarConexion() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        Connection connection=DriverManager.getConnection(this.url,this.user,this.password);
        System.out.println("Connection is Successful to the database "+url);
        return connection;         
    }
    
}