package com.mycompany.mavenconsultorio;

import java.sql.*;
import com.mycompany.conexion.Conexion;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Edgar
 */
public class Usuario {
    
    // Atributos
    private String Login;
    private String Nombre1;
    private String Nombre2;
    private String Apellido1;
    private String Apellido2;
    private String tipoId_idTipoId;
    private String identificacion;
    private String perfilUsuario_idPerfilUsuario;
    private String email;
    private String telefono;
    private String direccion;
    private String pais;
    
    // Constructor 

    public Usuario(String Login, String Nombre1, String Nombre2, String Apellido1, String Apellido2, String tipoId_idTipoId, String identificacion, String perfilUsuario_idPerfilUsuario, String email, String telefono, String direccion, String pais) {
        this.Login = Login;
        this.Nombre1 = Nombre1;
        this.Nombre2 = Nombre2;
        this.Apellido1 = Apellido1;
        this.Apellido2 = Apellido2;
        this.tipoId_idTipoId = tipoId_idTipoId;
        this.identificacion = identificacion;
        this.perfilUsuario_idPerfilUsuario = perfilUsuario_idPerfilUsuario;
        this.email = email;
        this.telefono = telefono;
        this.direccion = direccion;
        this.pais = pais;
    }
    
    public Usuario(){
        
    }

    // Metodos
    // getters ans setters

    public String getLogin() {
        return Login;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public String getNombre1() {
        return Nombre1;
    }

    public void setNombre1(String Nombre1) {
        this.Nombre1 = Nombre1;
    }

    public String getNombre2() {
        return Nombre2;
    }

    public void setNombre2(String Nombre2) {
        this.Nombre2 = Nombre2;
    }

    public String getApellido1() {
        return Apellido1;
    }

    public void setApellido1(String Apellido1) {
        this.Apellido1 = Apellido1;
    }

    public String getApellido2() {
        return Apellido2;
    }

    public void setApellido2(String Apellido2) {
        this.Apellido2 = Apellido2;
    }

    public String getTipoId_idTipoId() {
        return tipoId_idTipoId;
    }

    public void setTipoId_idTipoId(String tipoId_idTipoId) {
        this.tipoId_idTipoId = tipoId_idTipoId;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPerfilUsuario_idPerfilUsuario() {
        return perfilUsuario_idPerfilUsuario;
    }

    public void setPerfilUsuario_idPerfilUsuario(String perfilUsuario_idPerfilUsuario) {
        this.perfilUsuario_idPerfilUsuario = perfilUsuario_idPerfilUsuario;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    // toString. para ver los valores de los atributos de la clase

    @Override
    public String toString() {
        return "Usuario{" + "Login=" + Login + ", Nombre1=" + Nombre1 + ", Nombre2=" + Nombre2 + ", Apellido1=" + Apellido1 
                + ", Apellido2=" + Apellido2 + ", tipoId_idTipoId=" + tipoId_idTipoId + ", identificacion=" + identificacion 
                + ", perfilUsuario_idPerfilUsuario=" + perfilUsuario_idPerfilUsuario + ", email=" + email + ", telefono=" + telefono 
                + ", direccion=" + direccion + ", pais=" + pais + '}';
    }

    // CRUD
    // Guardar
    public void guardar() throws SQLException, ClassNotFoundException{ // ClassNotFoundException
        // clase de excepcion, de errores que puedan surgir
        Conexion con = new Conexion(); // instanciar la cone0xion
        // Sentencia que asigna los valores a la tabla usuarios
        String sql = "INSERT INTO usuarios (Login, Nombre1, Nombre2, Apellido1, Apellido2, tipoId_idTipoId, identificacion, "
                + "perfilUsuario_idPerfilUsuario, email, telefono, direccion, pais) VALUES ("
                +getLogin()+",'"+getNombre1()+"',"+getNombre2()+","+getApellido1()+","+getApellido2()+","+getTipoId_idTipoId()+","
                +getIdentificacion()+","+getPerfilUsuario_idPerfilUsuario()+","+getEmail()+","+getTelefono()+","+getDireccion()+","
                +getPais()+");"; // consulta a la BD
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
        //Conexion con = con.iniciarConexion();// conexion de tipo con
        //Statement statement = con.createStatement();
        //statement.execute(sql);
    }
    // Consultar
    public void consultar() throws SQLException, ClassNotFoundException{ // faltan las excepciones
        Conexion con = new Conexion(); // instanciar la conexion
        String sql = "SELECT Login, Nombre1, Nombre2, Apellido1, Apellido2, tipoId_idTipoId, identificacion, "
                + "perfilUsuario_idPerfilUsuario, email, telefono, direccion, pais) FROM usuarios";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);      
    }
    // Actualizar
    public void actualizar () throws SQLException, ClassNotFoundException{
        Conexion con = new Conexion(); // instanciar la conexion
        String sql = "UPDATE usuarios SET Login = "+getLogin()+",Nombre1 = "+getNombre1()+",Nombre2 = "+getNombre2()
                +",Apellido1 = "+getApellido1()+",Apellido2 = "+getApellido2()+",tipoId_idTipoId = "+getTipoId_idTipoId()
                +",identificacion = "+getIdentificacion()+",perfilUsuario_idPerfilUsuario = "+getPerfilUsuario_idPerfilUsuario()
                +",email = "+getEmail()+",telefono = "+getTelefono()+",direccion = "+getDireccion()+",pais = "+getPais()
                +"WHERE Login = "+getLogin()+";"; //#identificacion no se debe poder actualizar
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }
    // Borrar
    public void borrar() throws ClassNotFoundException, SQLException{
        Conexion con = new Conexion(); // instanciar la conexion
        String sql = "DELETE FROM usuarios WHERE Login ="+getLogin()+";";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);      
    }
    
}
