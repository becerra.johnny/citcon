/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenconsultorio;

import com.mycompany.conexion.Conexion;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Edgar
 */
public class Medicamentos {
    //Atributos
    private String idMedicamento;
    private String nomMedicamento;
    private String presentacionMedicamento;
    
    //Constructor por defecto
    
    public Medicamentos(String idMedicamento, String nomMedicamento, String presentacionMedicamento) {
        this.idMedicamento = idMedicamento;
        this.nomMedicamento = nomMedicamento;
        this.presentacionMedicamento = presentacionMedicamento;
    }
    
    //Constructor con los atributos
    public Medicamentos(){
        
    }
    
    //Metodos
    
    //Getters (obtener) and setters (establecer)
   
    public String getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(String idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public String getNomMedicamento() {
        return nomMedicamento;
    }

    public void setNomMedicamento(String nomMedicamento) {
        this.nomMedicamento = nomMedicamento;
    }

    public String getPresentacionMedicamento() {
        return presentacionMedicamento;
    }

    public void setPresentacionMedicamento(String presentacionMedicamento) {
        this.presentacionMedicamento = presentacionMedicamento;
    }
           
    //toString
    
    @Override
    public String toString() {
        return "Medicamentos{" + "idMedicamento=" + idMedicamento + ", nomMedicamento=" + nomMedicamento + ", presentacionMedicamento=" + presentacionMedicamento + '}';
    }
     
    //CRUD
    public void guardar() throws SQLException, ClassNotFoundException{ // ClassNotFoundException
        // clase de excepcion, de errores que puedan surgir
        Conexion con = new Conexion(); // instanciar la cone0xion
        // Sentencia que asigna los valores a la tabla usuarios
        String sql = "INSERT INTO medicamentos (idMedicamento, nomMedicamento, presentacionMedicamento) VALUES ('"
                +getIdMedicamento()+"','"+getNomMedicamento()+"','"+getPresentacionMedicamento()+"');"; // consulta a la BD
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    
    }
    // Consultar
    public void consultar() throws SQLException, ClassNotFoundException{ // faltan las excepciones
        Conexion con = new Conexion(); // instanciar la conexion
        String sql = "SELECT (idMedicamento, nomMedicamento, presentacionMedicamento) FROM medicamentos";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);      
    }
    // Actualizar
    public void actualizar () throws SQLException, ClassNotFoundException{
        Conexion con = new Conexion(); // instanciar la conexion
        String sql = "UPDATE medicamentos SET idMedicamento = '"+getIdMedicamento()+"', nomMedicamento = '"+getNomMedicamento()+"',presentacionMedicamento = '"+getPresentacionMedicamento()
                +"';";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);
    }
    // Borrar
    public void borrar() throws ClassNotFoundException, SQLException{
        Conexion con = new Conexion(); // instanciar la conexion
        String sql = "DELETE FROM medicamentos WHERE idMedicamento ='"+getIdMedicamento()+"';";
        Connection conex = con.iniciarConexion();
        Statement statement = conex.createStatement();
        statement.execute(sql);      
    }
}
